+++
title = "DRV10987V01: MLAB BLDC driver"
projectdeveloper = "MLAB-project team"
projecturl = "https://www.mlab.cz/module/DRV10987V01/"
"made-with-kicad/categories" = [
    "Motor Controller",
    "Development Board"
]
+++

The DRV10987V01 is an MLAB module featuring the DRV10987 controller by Texas Instruments, designed for sensorless operation of Brushless DC (BLDC) and Permanent Magnet Synchronous Motors (PMSM). It's a compact, powerful solution for driving motors with diverse voltage requirements, offering a continuous current of 2A and peak current of 3A. The module can control motor speed based on PWM, analogue, or digital (I2C) input, and has built-in functions for overload and overheat protection. It's ideal for applications like robotic systems, home appliances, and hobby projects.